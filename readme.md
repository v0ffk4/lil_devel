# Oh, hi 🤪

This is a little development environment for web. Here's how it works.

1. Install required node modules  
```
npm i
```
1. Launch 🚀  
```
npm start
```
1. In your web browser go to _http://localhost:3000_
1. Enjoy ☕️

Of course you should install  _[node.js](https://nodejs.org/en/)_ if u want this to work 😁
